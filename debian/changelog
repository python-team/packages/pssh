pssh (2.3.5-2) unstable; urgency=medium

  * Patch from upstream to fix pscp with port specification
    (Closes: #1035051).

 -- Hilmar Preusse <hille42@web.de>  Sat, 29 Apr 2023 23:06:12 +0200

pssh (2.3.5-1) unstable; urgency=medium

  * New upstream version, disable obsolete patches.
    - IPv6 host address processing unbroken (Closes: #741537)
  * Add pybuild-plugin-pyproject to BD, to build using pyproject.toml.

 -- Hilmar Preusse <hille42@web.de>  Thu, 27 Apr 2023 21:21:36 +0200

pssh (2.3.4-2) unstable; urgency=medium

  * Source only upload

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 27 Oct 2021 16:41:41 +0200

pssh (2.3.4-1) unstable; urgency=medium

  [ Hilmar Preusse ]
  * New upstream release, remove obsolete patches, refresh remaining.
    * It has support for parallel rsync and scp with multiple files:
      (Closes: #573177).
  * Apply patch for upstream issue #119 (Closes: #992515).
  * Really remove unused man pages from upstream.
  * Install HOWTOS from upstream (HTML & PDF).
  * Split python3-psshlib python modules into separate package
    (Closes: #830138).

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

  [ Jochen Sprickerhof ]
  * Use dh-sequence-python3
  * Bump policy versions (no changes)
  * remove gitlab-ci.yml
  * Fix section and typos
  * Depend on same version of library

 -- Hilmar Preusse <hille42@web.de>  Mon, 18 Oct 2021 23:42:39 +0200

pssh (2.3.1-4) unstable; urgency=medium

  * Fix typos in manual pages, thanks to Jakub Wilk
    (Closes: #803940, #804077).
  * Get commit 86e308c6bd62b3422d3e5a95ef1d330ce167171d from upstream
    (and adapt it for Debian specific man pages) (Closes: #804078).

 -- Hilmar Preusse <hille42@web.de>  Tue, 23 Feb 2021 00:02:07 +0100

pssh (2.3.1-3) unstable; urgency=low

  * https://github.com/lilydjwg/pssh is our new upstream
    (Closes: #891340), update d/watch file.
  * dh compat = 13.
  * Some more lintian cleanup.
  * Standards version 4.5.1, no changes needed.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Name, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.
  * d/watch: Use https protocol.

 -- Hilmar Preusse <hille42@web.de>  Mon, 22 Feb 2021 00:01:29 +0100

pssh (2.3.1-2) unstable; urgency=medium

  * Salvage and move it to PAPT.
    Thanks to Andrew Pollock for maintaining pssh! (Closes: #930780)
  * Rework packaging, move to Python 3 (Closes: #937329)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 05 Apr 2020 14:43:52 +0200

pssh (2.3.1-1) unstable; urgency=low

  * New upstream release
  * debian/watch: updated to use contribution from bartm
  * debian/control: bumped Standards-Version (no changes)
  * debian/{control,rules}: switch from dh_pysupport to dh_python2
  * debian/control: added dependency on openssh-client (closes: #721151)

 -- Andrew Pollock <apollock@debian.org>  Mon, 12 May 2014 09:43:22 +1000

pssh (2.2.2-1) unstable; urgency=low

  * New upstream release
  * debian/watch: update for current upstream location
  * debian/control: bumped Standards-Version
  * debian/rules: added build-arch and build-indep targets

 -- Andrew Pollock <apollock@debian.org>  Sat, 07 Jan 2012 21:21:29 -0800

pssh (2.1.1-1) unstable; urgency=low

  * New upstream release (closes: #570609)
  * debian/watch: updated for new upstream location
  * debian/control: bumped Standards-Version (no changes)
  * debian/rules: move pssh-askpass to /usr/lib/pssh

 -- Andrew Pollock <apollock@debian.org>  Sat, 06 Mar 2010 22:50:54 -0800

pssh (2.0-2) unstable; urgency=low

  * debian/rules: add --prefix=/usr to build to fix FTBFS with Python 2.6
    (closes #557958)

 -- Andrew Pollock <apollock@debian.org>  Wed, 25 Nov 2009 08:20:46 -0800

pssh (2.0-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: updated for new upstream
  * debian/control: updated for new upstream
  * removed patches for installed programs, they now dynamically emit their
    names in the usage messages
  * debian/control: removed build-dependency on quilt
  * debian/docs: removed, upstream no longer ships a TODO file

 -- Andrew Pollock <apollock@debian.org>  Tue, 03 Nov 2009 23:04:44 -0800

pssh (1.4.3-2) unstable; urgency=low

  * debian/rules: don't ship upstream's BUGS file (closes: #547298)
  * debian/control: bumped Standards-Version (no changes)
  * debian/control: add quilt to build dependencies
  * patch all installed programs so usage messages refer to the names used in
    debian to avoid file conflicts with the putty package (closes: #526801)
  * copied quilt's README.source to debian/README.source
  * debian/rules: include all of the upstream documentation
  * Added manpages contributed by Tatsuki Sugiura <sugi@nemui.org> (closes:
    #471602)

 -- Andrew Pollock <apollock@debian.org>  Wed, 21 Oct 2009 00:07:47 -0700

pssh (1.4.3-1) unstable; urgency=low

  * New upstream release

 -- Andrew Pollock <apollock@debian.org>  Tue, 28 Oct 2008 23:28:52 -0700

pssh (1.4.2-1) unstable; urgency=low

  * New upstream release

 -- Andrew Pollock <apollock@debian.org>  Sat, 04 Oct 2008 11:33:55 -0700

pssh (1.4.0-1) unstable; urgency=low

  * New upstream release

 -- Andrew Pollock <apollock@debian.org>  Mon, 25 Aug 2008 22:17:35 -0700

pssh (1.3.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: bumped Standards-Version (no changes)

 -- Andrew Pollock <apollock@debian.org>  Sun, 13 Jul 2008 11:39:50 -0700

pssh (1.3.1-6) unstable; urgency=low

  * Added debian/watch
  * debian/dirs: removed usr/sbin
  * debian/control: bumped Standards-Version (no changes)

 -- Andrew Pollock <apollock@debian.org>  Sat, 22 Mar 2008 22:03:48 -0700

pssh (1.3.1-5) unstable; urgency=low

  * debian/control: moved Homepage: from binary to source

 -- Andrew Pollock <apollock@debian.org>  Sat, 27 Oct 2007 14:44:37 -0700

pssh (1.3.1-4) unstable; urgency=low

  * debian/control: fix typo in description (closes: #434210)

 -- Andrew Pollock <apollock@debian.org>  Tue, 24 Jul 2007 16:15:10 -0700

pssh (1.3.1-3) unstable; urgency=low

  * debian/control: redo package description to be package compliant (gawd I
    suck) (closes: #434210)

 -- Andrew Pollock <apollock@debian.org>  Mon, 23 Jul 2007 22:50:29 -0700

pssh (1.3.1-2) unstable; urgency=low

  * Reworked package description (closes: #434210)

 -- Andrew Pollock <apollock@debian.org>  Sun, 22 Jul 2007 09:42:05 -0700

pssh (1.3.1-1) unstable; urgency=low

  * Initial release (Closes: #433812)

 -- Andrew Pollock <apollock@debian.org>  Wed, 18 Jul 2007 16:35:24 -0700
